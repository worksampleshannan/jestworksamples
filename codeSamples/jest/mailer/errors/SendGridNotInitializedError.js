class SendGridUninitializedError extends Error {
  constructor () {
    super('SendGrid has not been initialized yet')
  }
}

module.exports = SendGridUninitializedError
