class ParameterNotFoundError extends Error {
  constructor (parameterName) {
    super(`Parameter: "${parameterName}" not found!`)
    this.parameterName = parameterName
  }
}

module.exports = ParameterNotFoundError
