class ProviderEmailNotAvailable extends Error {
  constructor (store_unique_id, user_unique_id, provider_unique_id) {
    super(`
      store_unique_id: ${store_unique_id}
      user_unique_id: ${user_unique_id}
      provider_unique_id: ${provider_unique_id}
    `)

    this.name = ProviderEmailNotAvailable.name
  }
}

module.exports = ProviderEmailNotAvailable
