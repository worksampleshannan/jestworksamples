class EmailSenderInfoNotProvidedError extends Error {
  constructor (emailId) {
    super(`Unable to get sender information for ${emailId}`)
    this.emailId = emailId
    this.timestamp = Date.now()
  }
}

module.exports = EmailSenderInfoNotProvidedError
