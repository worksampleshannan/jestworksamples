class ReceiverEmailNotFound extends Error {
  constructor (receiverEmailParameter, debugInfo) {
    super()
    this.debugInfo = debugInfo
    this.receiverEmailParameter = receiverEmailParameter
  }
}

module.exports = ReceiverEmailNotFound
