const dashboardDynamicEmailContent = require('../dashboardEmailContent')

describe(dashboardDynamicEmailContent.name, () => {
  const tests = [
    {
      input: ['Hello XXXXXX', ['REDACTED']],
      expected: 'Hello REDACTED'
    },

    {
      input: ['Hello', []],
      expected: 'Hello'
    },

    {
      input: [''],
      expected: ''
    }
  ]

  test('test cases return expected output', () => {
    for (const t of tests) {
      expect(dashboardDynamicEmailContent.apply(null, t.input)).toBe(t.expected)
    }
  })

  test('throws if undefined template content is provided', () => {
    expect(() => dashboardDynamicEmailContent(undefined, [])).toThrow(new Error('no "emailContent" provided'))
  })
})
