jest.mock('../db')
jest.mock('../emailInfo')

const TEST_EMAIL_LABEL = 'TEST'

jest.mock('../baseEmailFunctions', () => ({
  [TEST_EMAIL_LABEL]: jest.fn(() => true)
}))

const emailInfo = require('../emailInfo')
const sendMail = require('../sendMail')
const db = require('../db')
const baseEmailFunctions = require('../baseEmailFunctions')

describe(sendMail.name, () => {
  describe('core', () => {
    beforeAll(() => {
      db.getSettings.mockReturnValue({})
      db.getEmailDetail.mockReturnValue({})
    })

    beforeEach(() => {
      jest.spyOn(console, 'warn')
      jest.resetAllMocks()
    })

    test('send if is_send is true', async () => {
      db.getEmailDetail.mockReturnValue({
        is_send: true
      })

      db.getSettings.mockReturnValue({
        sender_email: 'hello@example.com',
        sender_name: 'Hannan'
      })

      await sendMail(TEST_EMAIL_LABEL, {})

      expect(emailInfo.getEmailInfo).toHaveBeenCalledTimes(1)
      expect(db.getSettings).toHaveBeenCalledTimes(1)
      expect(db.getEmailDetail).toHaveBeenCalledTimes(1)
      expect(baseEmailFunctions.TEST).toHaveBeenCalledTimes(1)
    })

    test('do not send if is_send is false', async () => {
      db.getEmailDetail.mockReturnValue({ is_send: false })
      jest.spyOn(console, 'info')

      const returnValue = await sendMail(TEST_EMAIL_LABEL, {})

      expect(baseEmailFunctions.TEST).toHaveBeenCalledTimes(0)
      expect(returnValue).toBeUndefined()
      expect(console.info).toHaveBeenCalledTimes(1)
    })
  })
})
