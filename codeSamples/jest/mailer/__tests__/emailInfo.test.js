const mongooseMock = require('../../../../__mocks__/mongoose')
const emailInfo = require('../emailInfo')

describe('getEmailInfo', () => {
  const emailDetail = mongooseMock.model('email_detail_v1').findOne().toObject()
  const setting = mongooseMock.model('setting').findOne().toObject()

  test('if emailDetail is missing', () => {
    expect(emailInfo.getEmailInfo(undefined, setting)).toBeUndefined()
  })

  test('if setting is missing', () => {
    expect(emailInfo.getEmailInfo(emailDetail, undefined)).toBeUndefined()
  }),

  test('output for appropriate arguments email_detail sender source', () => {
    expect(emailInfo.getEmailInfo(
      {
        _id: { $oid: '60c758bcef724b190ee24d10' },

        email_unique_title: 'USER_OTP_VERIFICATION',
        email_title: 'Your OTP Verification',
        email_content: '<p>Email Verification code to complete your registration process is : XXXXXX.</p>',
        sendgrid_template_id: 'jlkj',

        sender_name: 'REDACTED Admin',
        sender_email: 'admin@REDACTED.app',

        primary_locale: 'en-UK',

        is_send: true,

        sendgrid_template_id: '',

        reply_to_list: [
          {
            name: 'ABC',
            email: 'abc@bbc.com'
          }
        ]
      },
      {
        default_sendgrid_sender_email: 'hannan@REDACTED.app',
        default_sendgrid_sender_name: 'hannan',
        default_sendgrid_sender_replyto: 'hannan@REDACTED.app',
        sendgrid_api_key: process.env.SENDGRID_API_KEY
      }
    )).toMatchSnapshot()
  })
})
