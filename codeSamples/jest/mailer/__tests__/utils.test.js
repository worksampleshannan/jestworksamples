const ParameterNotFoundError = require('../errors/ParameterNotFoundError')
const { parametersValidator } = require('../utils')

describe('utils', () => {
  describe('parametersValidator', () => {
    test('parameterNames is an empty array', () => {
      expect(parametersValidator([], {})).toBeFalsy()
    })

    test('when param is not passed', () => {
      const parameterName = 'param'
      expect(() => {
        parametersValidator([parameterName], { notParam: 1 })
      }).toThrow(new ParameterNotFoundError(parameterName))
    })

    test('when param is passed', () => {
      expect(parametersValidator(['param'], { param: 1 })).toBeFalsy()
    })
  })
})
