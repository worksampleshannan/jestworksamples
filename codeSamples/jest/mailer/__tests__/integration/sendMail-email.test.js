const mongoose = require('mongoose')
const sendgrid = require('../../sendgrid')
const sendMail = require('../../sendMail')
const sendMailFixtures = require('./fixtures/sendMailFixtures')

jest.setTimeout(30 * 1000)

describe('integration test sendMail non-mocked', () => {
  beforeAll(async () => {
    const Setting = mongoose.model('setting')
    const settingDoc = await Setting.findOne({})
    console.log(settingDoc.sendgrid_api_key)
    sendgrid.initialize(settingDoc.sendgrid_api_key)
  })

  for (const [mailId, rawParameters] of sendMailFixtures.rawParameters) {
    test(`fixture ${mailId}`, async () => {
      const resolvedParameters = await sendMailFixtures.getParameters(rawParameters)
      const result = await sendMail(mailId, resolvedParameters)

      expect(result).toBeTruthy()
    })
  }
})
