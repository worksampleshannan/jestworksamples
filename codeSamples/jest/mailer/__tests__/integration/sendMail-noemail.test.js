jest.mock('@sendgrid/mail')

const sendgridAdapter = require('../../sendgrid')
const sendgrid = require('@sendgrid/mail')
const sendMail = require('../../sendMail')
const sendMailFixtures = require('./fixtures/sendMailFixtures')

describe('integration test sendMail mocked', () => {
  beforeAll(() => {
    sendgridAdapter.initialize('SG.ada')
  })

  for (const [mailId, rawParameters] of sendMailFixtures.rawParameters) {
    test(`fixture ${mailId}`, async () => {
      const resolvedParameters = await sendMailFixtures.getParameters(rawParameters)
      const result = await sendMail(mailId, resolvedParameters)

      expect(sendgrid.send).toHaveBeenCalledTimes(1)
      expect(result).toBe(undefined)
    })
  }
})
