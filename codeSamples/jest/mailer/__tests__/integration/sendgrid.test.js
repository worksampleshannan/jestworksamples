const faker = require('faker')
const sendgrid = require('../../sendgrid')

const testTemplateId = 'd-f3de3478204c45ceb2f3e09b58faba70'

const defaultTo = {
  name: 'Hannan',
  email: 'me@example.app'
}

const defaultFrom = defaultTo
const defaultEmailTitle = faker.lorem.words(10)
const defaultEmailContent = faker.lorem.words(100)

const fullyDefinedFixture = {
  email_info: {
    sender_info: {
      ...defaultFrom,

      reply_to: {
        name: 'info',
        email: 'info@example.app'
      }

    },
    sendgrid_template_id: testTemplateId,
    email_title: defaultEmailTitle
  },

  to: defaultTo,

  dynamicTemplateData: {
    email_title: defaultEmailTitle,
    email_content: defaultEmailContent
  }
}

/**
 * SendGrid Autentication
 */
describe('sendgrid', () => {
  describe('sendDynamicMailUsingEmailInfo', () => {
    beforeEach(async () => {
      sendgrid.initialize('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')
    })

    test('everything is defined', async () => {
      const result = await sendgrid.sendDynamicEmailUsingEmailInfo(
        fullyDefinedFixture.to,
        fullyDefinedFixture.email_info,
        fullyDefinedFixture.dynamicTemplateData
      )

      expect(typeof result[0]).toBe('object')
      expect(result[0].statusCode).toBe(202)
      expect(result[0].messageId).toBeTruthy()
    })

    test('few parameters of dynamicTemplateData are missing', async () => {
      const result = await sendgrid.sendDynamicEmailUsingEmailInfo(
        fullyDefinedFixture.to,
        fullyDefinedFixture.email_info,
        {
          ...fullyDefinedFixture.dynamicTemplateData,
          email_content: undefined
        }
      )

      expect(typeof result[0]).toBe('object')
      expect(result[0].statusCode).toBe(202)
      expect(result[0].messageId).toBeTruthy()
    })

    // SendGrid Template ID not Found
    test('sendgrid_template_id is missing', async () => {
      let err
      try {
        const result = await sendgrid.sendDynamicEmailUsingEmailInfo(
          fullyDefinedFixture.to,
          {
            ...fullyDefinedFixture.email_info,
            sendgrid_template_id: undefined
          },

          fullyDefinedFixture.dynamicTemplateData
        )
      } catch (error) {
        err = error
      }

      expect(err).toBeTruthy()
    })

    test('for reply_to_list instead of reply_to', async () => {
      const result = await sendgrid.sendDynamicEmailUsingEmailInfo(
        fullyDefinedFixture.to,
        {
          ...fullyDefinedFixture.email_info,
          sender_info: {
            ...fullyDefinedFixture.email_info.sender_info,
            reply_to: undefined,
            reply_to_list: [
              fullyDefinedFixture.email_info.sender_info.reply_to
            ]
          }
        },
        fullyDefinedFixture.dynamicTemplateData
      )

      expect(typeof result[0]).toBe('object')
      expect(result[0].statusCode).toBe(202)
      expect(result[0].messageId).toBeTruthy()
    })
  })
})
