const mongoose = require('mongoose')
const mailEvents = require('../../mailEvents')
const EmailDetail = mongoose.model('email_detail_v1')

describe('Check EmailDetail Doc availability for all mailEvents', () => {
  for (const mailEvent of Object.keys(mailEvents)) {
    test(`is available ${mailEvent}?`, async () => {
      const emailDoc = await EmailDetail.findOne({ email_unique_title: mailEvent })
      expect(emailDoc).toBeTruthy()
    })
  }
})
