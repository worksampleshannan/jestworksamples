const currencySymbolMap = require('currency-symbol-map')
const mongoose = require('mongoose')
const mailEvents = require('../../../mailEvents')

const baseMailFunctions = require('../../../baseEmailFunctions/index')

const {
  storeDashboardTemplateEmail,
  providerDashboardTemplateEmail,
  userDashboardTemplateEmail
} = require('../../../baseEmailFunctions/dashboardTemplateEmails')

const DEFAULT_CURRENCY = currencySymbolMap('GBP')

const providerWeeklyEarning = {
  statement_number: 'DHABJDAKJQEIO',
  start_date: new Date().toISOString(),
  end_date: new Date(Date.now() + 240000000).toISOString(),
  total_order_price: 500,
  total_provider_profit: 3,
  total_provider_earning: 4,
  total_wallet_income_set: 13,
  total_pay_to_provider: 1313,
  total_provider_paid_order_payment: 20,
  total_provider_have_cash_payment: 200,
  total_provider_have_cash_payment_on_hand: 30,
  total_wallet_income_set_in_cash_order: 200,
  total_wallet_income_set_in_other_order: 2000
}

const storeWeeklyEarning = {
  statement_number: Math.random().toString().substr(2),
  start_date: new Date().toISOString(),
  end_date: new Date().toISOString(),
  total_store_profit: 20.0,
  store_have_service_payment: true,
  store_have_order_payment: true,
  total_store_earning: Math.round(Math.random() * 13),
  total_wallet_income_set_in_other_order: 100 * Math.random(),
  total_wallet_income_set_in_cash_order: 200 * Math.random(),
  total_wallet_income_set: 300 * Math.random(),
  total_pay_to_store: 500 * Math.random()
}

const staticData = {
  user: { _id: { $oid: '61a4b9388bfe712b595f6d95' }, favourite_stores: [], payment_intent_id: '', customer_id: 'cus_KgV3wK51dLrd8A', image_url: '', comments: 'New Register', first_name: 'Bharat', last_name: 'New', email: 'hannan@REDACTED.app', otp: '', password: 'REDACTED', social_id: '', social_ids: [], login_by: 'manual', country_phone_code: 'REDACTED', country_code: 'GB', app_version: '1.0.10', phone: 'REDACTED', address: '', city: '', device_token: '', device_type: 'ios', server_token: 'oMCMKXRhOwoLIVhTdLbP7hkvF5risiGv', orders: [], promo_count: 0, referral_code: 'GBP63R18BR7', is_referral: false, total_referrals: 0, store_rate: 0, store_rate_count: 0, provider_rate: 0, provider_rate_count: 0, wallet: 0, wallet_currency_code: 'GBP', is_use_wallet: false, is_approved: true, is_email_verified: false, is_phone_number_verified: false, is_document_uploaded: true, is_user_type_approved: false, is_deactivate: false, user_type: 1, admin_type: 7, user_type_id: null, country_id: { $oid: '60ca03351863b34d759c8a81' }, created_at: { $date: '2021-11-29T11:27:52.058Z' }, updated_at: { $date: '2021-11-30T11:18:17.529Z' }, unique_id: 173, __v: 0, cart_id: null },
  provider: { _id: { $oid: '6101e4dcf23a72225f45e68e' }, customer_id: '', vehicle_ids: [{ $oid: '6101e76e6ce33020604abd89' }], service_id: [], bank_id: 'ba_1JINI8RIHciZkgxNrzkiF5lu', account_id: 'acct_1JINI6RIHciZkgxN', provider_type_id: null, image_url: 'provider_profiles/REDACTED.jpg', comments: 'New Register', first_name: 'REDACTED', last_name: 'REDACTED', email: 'hannan@REDACTED.app', otp: '', password: '$2b$10$wZTmo6o9LKlRpTH13.jqhOeWhYux85TRxaReDb.iaF0ZkAQETd/rm', social_id: '', social_ids: [], login_by: 'manual', app_version: '1.0', country_phone_code: 'REDACTED', phone: '7732664436', address: '', payment_intent_id: '', vehicle_model: '', vehicle_number: '', device_token: 'D8AA9D1C7093C9E74EEB6C12D82F56655CECD4556285F9A67800CA671FC6E60B', device_type: 'ios', server_token: 'mKsOWluFXLEHkIlbLHd9gRByaIXnAdln', user_rate: 0, user_rate_count: 0, store_rate: 0, store_rate_count: 0, referral_code: 'GB6DANA50I', wallet: 0, wallet_currency_code: 'GBP', total_referrals: 0, requests: [], current_request: [], total_requests: 0, total_accepted_requests: 0, total_rejected_requests: 0, total_cancelled_requests: 0, total_completed_requests: 0, total_online_time: 0, total_active_job_time: 0, is_approved: false, is_active_for_job: false, is_online: false, is_in_delivery: false, is_email_verified: false, is_phone_number_verified: false, is_document_uploaded: true, is_provider_type_approved: false, is_deactivate: false, admin_type: 8, provider_type: 1, country_id: { $oid: '60ca03351863b34d759c8a81' }, city_id: { $oid: '60d05d883e1db012bb703396' }, location_updated_time: { $date: '2021-07-28T23:14:36.070Z' }, created_at: { $date: '2021-07-28T23:14:36.070Z' }, updated_at: { $date: '2021-07-29T00:40:09.347Z' }, unique_id: 41, __v: 1, selected_bank_id: { $oid: '6101f8e9f23a7280dc45eb48' } },
  order_payment: { _id: { $oid: '61a604f9f9f9e85c26917f90' }, payment_intent_id: 'pi_3K1UB7EaMvpFWPrv09mxM9G4', capture_amount: 2, tip_amount: 0, tip_value: 0, promo_id: null, delivery_price_used_type: 1, delivery_price_used_type_id: null, currency_code: 'GBP', admin_currency_code: 'GBP', order_currency_code: 'GBP', current_rate: 1, wallet_to_admin_current_rate: 1, wallet_to_order_current_rate: 1, total_distance: 2.562534004, total_time: 9.97, total_item_count: 1, is_distance_unit_mile: true, service_tax: 0, total_service_price: 1.56, total_admin_tax_price: 0, total_delivery_price: 2, pay_to_provider: 0, admin_profit_mode_on_delivery: 1, admin_profit_value_on_delivery: 100, total_admin_profit_on_delivery: 2, total_provider_income: 0, item_tax: 0, total_cart_price: 0, total_store_tax_price: 0, total_order_price: 0, other_promo_payment_loyalty: 0, pay_to_store: 0, admin_profit_mode_on_store: 0, admin_profit_value_on_store: 0, total_admin_profit_on_store: 0, total_store_income: 0, promo_payment: 0, user_pay_payment: 2, wallet_payment: 0, total_after_wallet_payment: 2, remaining_payment: 0, total: 2, cash_payment: 0, card_payment: 2, google_payment: 0, apple_payment: 0, is_paid_from_wallet: false, is_promo_for_delivery_service: true, is_payment_mode_cash: false, is_payment_paid: true, is_order_price_paid_by_store: true, is_store_pay_delivery_fees: false, is_min_fare_applied: true, is_transfered_to_store: false, is_transfered_to_provider: false, is_user_pick_up_order: false, is_order_payment_status_set_by_store: false, is_cancellation_fee: false, order_cancellation_charge: 0, is_order_payment_refund: false, refund_amount: 0, is_provider_income_set_in_wallet: false, is_store_income_set_in_wallet: false, provider_income_set_in_wallet: 0, store_income_set_in_wallet: 0, completed_date_tag: '', completed_date_in_city_timezone: null, cart_id: { $oid: '61a604f58bfe71f0905f6daf' }, store_id: null, user_id: { $oid: '61a0af6c50e1b163d17be396' }, country_id: { $oid: '60ca03351863b34d759c8a81' }, city_id: { $oid: '60d05d883e1db012bb703396' }, created_at: { $date: '2021-11-30T11:03:21.178Z' }, updated_at: { $date: '2021-11-30T11:07:22.159Z' }, unique_id: 532, __v: 0, payment_id: { $oid: '586f7db95847c8704f537bd5' }, invoice_number: 'ED OE 30112021 0000462', order_id: { $oid: '61a605ea8bfe7169e95f6db0' }, order_unique_id: 462 },
  store: { _id: { $oid: '60faa4ca8f888d42f2a1622d' }, customer_id: '', franchise_id: null, payment_intent_id: '', name: ['Mint Beauty'], delivery_locations: [], email: 'hannan@REDACTED.app', otp: '', country_phone_code: 'REDACTED', phone: '111111111', social_ids: [], password: 'REDACTED', address: 'REDACTED', image_url: 'store_profiles/60faa4ca8f888d42f2a1622dqN07.jpg', price_rating: 1, is_store_busy: false, is_business: true, is_approved: true, is_edit_menu: true, is_edit_menu_price: true, is_edit_menu_image: true, is_email_verified: false, is_phone_number_verified: false, is_document_uploaded: true, is_use_item_tax: false, item_tax: 0, min_order_price: 0, max_item_quantity_add_by_user: 0, is_order_cancellation_charge_apply: false, order_cancellation_charge_for_above_order_price: 0, order_cancellation_charge_type: 1, order_cancellation_charge_value: 0, is_taking_schedule_order: false, inform_schedule_order_before_min: 0, schedule_order_create_after_minute: 0, is_ask_estimated_time_for_ready_order: false, is_provide_pickup_delivery: false, is_provide_delivery_anywhere: true, delivery_radius: 0, is_store_pay_delivery_fees: false, free_delivery_for_above_order_price: 0, free_delivery_within_radius: 0, delivery_time: 30, delivery_time_max: 45, user_rate: 0, user_rate_count: 0, provider_rate: 5, provider_rate_count: 1, admin_profit_mode_on_store: 1, admin_profit_value_on_store: 0, is_visible: true, wallet: 24.49, wallet_currency_code: 'GBP', bank_id: '', account_id: '', is_store_can_add_provider: false, is_store_can_complete_order: false, store_time: [{ is_store_open: true, is_store_open_full_time: true, day: 0, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 1, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 2, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 3, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 4, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 5, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 6, day_time: [] }], is_store_set_schedule_delivery_time: false, store_delivery_time: [{ is_store_open: true, is_store_open_full_time: true, day: 0, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 1, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 2, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 3, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 4, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 5, day_time: [] }, { is_store_open: true, is_store_open_full_time: true, day: 6, day_time: [] }], website_url: '', slogan: '', offers: [], famous_products_tags: [], languages_supported: [{ code: 'en', is_visible: true, name: 'English', string_file_path: './uploads/i18n/en.json' }], comments: 'New Register', referral_code: 'GBBY8T0UA3', total_referrals: 0, is_referral: false, device_token: 'a623a0b1-ad29-f3f-0f07-76b68143d102', device_type: 'web', server_token: 'rd4ldfbKzd3YxlsVFyU6htFpQuKP55Zn', login_by: 'manual', store_type: 1, admin_type: 2, store_type_id: null, store_delivery_id: { $oid: '60d06a9b3e1db012bb7033b2' }, country_id: { $oid: '60ca03351863b34d759c8a81' }, city_id: { $oid: '60d05d883e1db012bb703396' }, location: [52.48126, -1.9055123], created_at: { $date: '2021-07-23T11:15:22.283Z' }, updated_at: { $date: '2021-10-26T16:44:22.827Z' }, unique_id: 1, __v: 0, branchio_url: '', is_deactivate: false }
}

const rawParameters = [
  [
    mailEvents.PROVIDER_INVOICE,

    {
      order_payment: 'mongoose_model_order_payment',
      user: 'mongoose_model_user',
      store: 'mongoose_model_store',
      currency: DEFAULT_CURRENCY,
      provider: 'mongoose_model_provider'
    }
  ],

  [
    mailEvents.PROVIDER_WEEKLY_INVOICE,

    {
      provider_weekly_earning: providerWeeklyEarning,
      provider: 'mongoose_model_provider'
    }
  ],

  [
    mailEvents.USER_INVOICE,

    {
      provider: 'mongoose_model_provider',
      order_payment: 'mongoose_model_order_payment',
      user: 'mongoose_model_user',
      currency: DEFAULT_CURRENCY,
      store: 'mongoose_model_store'
    }
  ],

  [
    mailEvents.STORE_INVOICE,

    {
      provider: 'mongoose_model_provider',
      store: 'mongoose_model_store',
      order_payment: 'mongoose_model_order_payment',
      currency: DEFAULT_CURRENCY,
      user: 'mongoose_model_user'
    }
  ],

  [
    mailEvents.STORE_WEEKLY_INVOICE,

    {
      store: 'mongoose_model_store',
      store_weekly_earning: storeWeeklyEarning
    }
  ],

  ...generateGenericTemplateFixtures()
]

async function getParameters (rawParameters) {
  const newObjectParameters = {}

  for (const [key, value] of Object.entries(rawParameters)) {
    if (typeof value === 'string' && value.startsWith('mongoose_model')) {
      const matchedValue = value.match(/mongoose_model_(.*)/)[1]
      newObjectParameters[key] = await getRandomDocFromModel(matchedValue)
      continue
    }

    newObjectParameters[key] = value
  }

  return newObjectParameters
}

function generateGenericTemplateFixtures () {
  const fixtures = []

  for (const [key, fn] of Object.entries(baseMailFunctions)) {
    const randomParameter = Math.random().toString(36).toUpperCase().slice(10)

    if (fn === storeDashboardTemplateEmail) {
      fixtures.push([key, {
        genericTemplateParam: [randomParameter],
        store: staticData.store
      }])
    }

    if (fn === providerDashboardTemplateEmail) {
      fixtures.push([
        key,
        {
          genericTemplateParam: [randomParameter],
          provider: staticData.provider
        }
      ])
    }

    if (fn === userDashboardTemplateEmail) {
      fixtures.push([
        key,
        {
          genericTemplateParam: [randomParameter],
          user: staticData.user
        }
      ])
    }
  }

  return fixtures
}

async function getRandomDocFromModel (modelName) {
  const model = mongoose.model(modelName)

  if (staticData[modelName]) {
    return staticData[modelName]
  }

  const count = await model.find({}).count()
  const randomDocNumber = Math.floor(count * Math.random())
  const docs = await model.find({}).skip(Math.max(randomDocNumber - 1, 0)).limit(1).exec()
  return docs[0].toJSON()
}

module.exports = {
  getParameters,
  getRandomDocFromModel,
  rawParameters
}
