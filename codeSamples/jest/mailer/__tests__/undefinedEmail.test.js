describe('undefinedEmail', () => {
  beforeEach(() => {
    jest.spyOn(console, 'warn').mockImplementation()
  })

  test('console.warn called', () => {
    expect(console.warn).toHaveBeenCalledLastTimes(1)
  })
})
