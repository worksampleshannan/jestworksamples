module.exports = {
  getEmailInfo: jest.fn(() => ({
    _id: { $oid: '60c758bcef724b190ee24d10' },

    email_unique_title: 'USER_OTP_VERIFICATION',
    email_title: 'Your OTP Verification',
    email_content: '<p>Email Verification code to complete your registration process is : XXXXXX.</p>',
    sendgrid_template_id: 'jlkj',

    sender_name: 'Redacted Admin',
    sender_email: 'admin@Redacted.app',

    primary_locale: 'en-UK',

    is_send: true,

    sendgrid_template_id: '',

    reply_to_list: [
      {
        name: 'ABC',
        email: 'abc@bbc.com'
      }
    ],

    sender_info: {
      name: 'Redacted Admin',
      email: 'admin@Redacted.app',
      reply_to_list: [
        {
          name: 'ABC',
          email: 'abc@bbc.com'
        }
      ],
      sender_source: 'email_detail'
    }
  }))
}
