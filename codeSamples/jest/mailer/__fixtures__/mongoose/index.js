const mongooseDummy = require('mongoose-dummy')
const path = require('path')
const modelsRoot = '../../../../models/'

const models = [
  'user/order_payment',
  'provider/provider',
  'store/store',
  'user/user'
]

function getModelsFixtures () {
  const fixturesObject = {}

  for (const model of models) {
    const model = require(path.join(modelsRoot, model))
    fixturesObject[model] = mongooseDummy(model.name)
  }

  return fixturesObject
}

module.exports = getModelsFixtures()
