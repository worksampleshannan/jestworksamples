/**
 * 
 * codeSample.test.js
 * 
 * This code sample tests for a signRequest function that signs a request using a given algorithm
 * This code makes use of Dynamic Unit Tests creating in JavaScript as well as extends the globalThis with a WebCrypto API 
 * alternative not available in Jest natively
 */


import { Crypto } from '@peculiar/webcrypto'
import signRequest from '../src/utils/signRequest'

const crypto = new Crypto()

interface SignRequestTestCase {
    input: [Record<string, any>, string],
    expected: string
}

describe('getSignature', () => {
    beforeAll(() => {
        Object.defineProperty(globalThis, 'crypto', {
            value: crypto
        })
    })

    const tests: SignRequestTestCase[] = [
        {
            input: [
                {
                    api_key: 'abcdefghijklmnopqr',
                    timestamp: '12345678910'
                },

                'aaaaaaabbbbbbbbccccccddddddeeeefffgg'
            ],

            expected: '56f2e19235c79b925e9839d862e1fb43a9e678426849e8f6a3c83ca4b03d4e59'
        },

        {
            input: [{
                "coin": "ETH",
                "api_key": "REDACTED",
                "timestamp": 123456789
            },


                'F4wq13123qdfsaBkqIsafcV3kc1fsJHfsabMqfsay4bWsUMuZ8bR64X9Ht'
            ],

            expected: '457b8992726asdsa6ba8ba42289d9872c7d073e8d2722301917ffd77bae03dcae0955'
        },

        {
            input: [
                {
                    "coin": "ETH",
                    "api_key": "REDACTED",
                    "timestamp": 123456789
                },

                'F4wq13123qdfsaBkqIsafcV3kc1fsJHfsabMqfsay4bWsUMuZ8bR64X9Ht'
            ],

            expected: '457b89927266ba8ba42289d912321872c7d073e8d2722301917ffd77bae03dcae0955'

        },

        {
            input: [
                {
                    "coin": "USDT",
                    "api_key": "REDACTED",
                    "timestamp": 123456789
                },

                'F4wq13123qdfsaBkqIsafcV3kc1fsJHfsabMqfsay4bWsUMuZ8bR64X9Ht'
            ],

            expected: 'b4ed818645dfac6c02312bb889de7091247370972f2ec37cae41b052820e4d96dec'
        },

        {
            input: [
                {
                    "coin": "BTC",
                    "api_key": "REDACTED",
                    "timestamp": 123456789
                },

                'F4wq13123qdfsaBkqIsafcV3kc1fsJHfsabMqfsay4bWsUMuZ8bR64X9Ht'
            ],

            expected: 'e924e6265e6fea6a47882313104cd6567115f879bdc0e71361cd44e28cb9d8bac7c4'
        },
    ]

    for (let i = 0; i < tests.length; i++) {
        const testCase = tests[i]

        test(`Test Case: ${i}`, async () => {
            expect(await signRequest(new URLSearchParams(testCase.input[0]), testCase.input[1])).toBe(testCase.expected)
        })
    }
})