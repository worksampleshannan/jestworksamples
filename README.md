# Jest Testing Code Samples 

Sharing some of the test code samples from my past projects. The samples include both Unit Tests and Integration Tests and are based on my open source and private work.

## Jest Local Code Samples


|Code File/Folder|Description|TestTypesIncluded|Redacted|
|---|---|---|---|
|[signRequest.ts](codeSamples/jest/signRequest.ts)|Units Tests a HTTP Request Header signing function|Unit Tests|Yes|
|[mailer](codeSamples/jest/mailer)|Tests for a Sendgrid Integration written in Node.js. The code itself is excluded and only the test cases are included. This project was for a legacy codebase, with a very complex deployment environment, hence to eliminate the amount of flaws in production I volunteerily implemented the tests cases for the module I was building|Unit Tests + Integration Tests|Yes|

### Open Source Conrtributions

Here are some of the open source contributions regarding testing that were made 

|Repository|Pull Request Link|Merge Status|Description|
|---|---|---|---|
|https://github.com/snyk/nodejs-lockfile-parser|https://github.com/snyk/nodejs-lockfile-parser/pull/112|Open|This is a feature Pull Request and as part of it I did relevant code modifications to the test files. The PR hasn't been merged yet due to the Snyk's Internal Project Timeline|


### Commitment to making the fullest out of the testing framework

The above given code samples are using subset of features that Jest offers, but based on the usecase and scope of the project I can use more of the Jest's capabilities to solve a testing problem efficiently.

Where possible I also try not to use more 3rd libraries apart from the testing framework itself to minimize the learning curve for others starting on the project, which I will also keep in mind while doing this project for you.

### Thank you! 

Thank you for taking a look at this sample and I look forward to hearing from you. 